<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {


	 public function __construct()
        {
                parent::__construct();

                // Your own constructor code
                error_reporting(0);
        }




	public function login()
	{
		$username = $this->session->userdata('username');
       	if($username){
       		$this->load->helper('url');
            $bu = base_url();
            redirect($bu.'profile');
        }


		$data['title'] = 'Login';
		$this->load->view('templates/header', $data);
		$this->load->view('auth/login');
		$this->load->view('templates/footer');

	}


	public function login_request() 
	{
		//error_log(0);
		$name = $this->input->post('email', TRUE);
        $password = $this->input->post('password', TRUE);
        $ip = $_SERVER['REMOTE_ADDR'];
		$this->load->model('signin_model');
		$this->load->model('app_model');


		// LOG IP
		$logIp = $this->signin_model->log_ip($name, $ip, $query);

		if($logIp > 0) {

			echo "3"; // BLOCKED LOGIN ATTEMPT
			die;

        } else {

          
				//check login conditionals

				$query = $this->signin_model->check_login($name, $password);


				// check login
				if ($query > 0) {

					// CREATE TOKEN AND SAVE
		            $this->session->set_userdata('username', $name);

		            // save token
					$returnedToken = $this->app_model->createTokenWeb($name);
					$this->session->set_userdata('token', $returnedToken);
					

					echo "1";


		            

				} else {
					
					//return error
					echo "0";
					

				}

		}



	}

	public function secret_request() 
	{
		$login_attempt_hash = $this->session->userdata('login_attempt_hash');
		$login_attempt_name = $this->session->userdata('login_attempt_name');
		$secret_word = $this->input->post('secret');
		$this->load->model('signin_model');

		if($login_attempt_hash AND $login_attempt_name) {
			$query = $this->signin_model->check_secret($login_attempt_hash, $login_attempt_name, $secret_word);
			if($query == 1) {
					$this->session->set_userdata('username', $login_attempt_name);
			} else {
					echo "0";
			}
			
		}
	}


	public function signup()
	{
		$data['title'] = 'Sign Up';
		$this->load->view('templates/header', $data);
		$this->load->view('auth/join');
		$this->load->view('templates/footer');
	}


	public function signup_request() 
	{
		$name = $this->input->post('name', TRUE);
        $email = $this->input->post('email', TRUE);
        $password = $this->input->post('password');


		$this->load->model('signup_model');

		//log ip address
		$query = $this->signup_model->log_ip();

		//check login conditionals
		$query = $this->signup_model->check_login($name);
		print($query);

		// check login
		if ($query == 1) {

			// create_account
			$this->signup_model->create_account($name, $email, $password);

			//set session
			$this->signup_model->proccess_login($name);
			return 1;

		} else {
			return 0;
		}
	}


	public function passwordRestore()
	{
		$data['title'] = 'Restore your password';
		$this->load->view('templates/header', $data);
		$this->load->view('auth/forgot_password');
		$this->load->view('templates/footer');
	}


	public function secretRestore()
	{
		$data['title'] = 'Restore your secret phrase';
		$this->load->view('templates/header', $data);
		$this->load->view('auth/forgot_secret');
		$this->load->view('templates/footer');
	}



	public function fastSignup()
	{

	
		$username = $this->session->userdata('username');
       	if($username){
       		echo "5"; // already logged in
       		die;
       	}


		$email = $this->input->post('email', TRUE);
        $password = $this->input->post('password', TRUE);	

        $this->load->model('signup_model');
        $this->load->model('email_model');
        $this->load->model('app_model');

        $query = $this->signup_model->fastSignup($email, $password);

        if($query == "1") {
        	echo "1";
        	$returnedToken = $this->app_model->createTokenMain($email);		
			$this->session->set_userdata('token', $returnedToken);
	        // SEND WELCOME EMAIL
	        $querytwo = $this->email_model->sendWelcome($email);
        } else {
        	echo $query;
        }

        

	}
	

	public function logout() 
	{
		$this->load->helper('url');
		$home_url = base_url();

		$this->session->unset_userdata('username');
		$this->session->unset_userdata('token');

		redirect($home_url.'/');
	}
	
}