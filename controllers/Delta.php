<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Delta extends CI_Controller {



	public function __construct()
        {
                parent::__construct();
                error_reporting(0);
                
        }


    public function index()
	{

        $data['title'] = 'Home';
        $this->load->view('templates/new/header', $data);
        $this->load->view('products/delta/delta');
        $this->load->view('templates/new/footer');

    }


    public function next()
    {

        $data['title'] = 'Home';
        $this->load->view('templates/new/header', $data);
        $this->load->view('products/delta/next');
        $this->load->view('templates/new/footer');

    }



    public function sendLink()
    {

        $this->load->library('twilio');

        $from = '+19292961554';
        $to = $this->input->post('n', TRUE);
        $message = "Hi, here's a link to download Kryptaxe app - http://onelink.to/d2hfmv";

        $response = $this->twilio->sms($from, $to, $message);


        if($response->IsError)
            echo 'Error: ' . $response->ErrorMessage;
        else
            echo 'Sent message to ' . $to;

    }


    public function test()
    {
        echo CI_VERSION;
    }



}