<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {



	public function __construct()
        {
                parent::__construct();
                error_reporting(0);
                
        }


    public function team()
	{

        $data['title'] = 'Team';
        $this->load->view('templates/new/header', $data);
        $this->load->view('pages/team');
        $this->load->view('templates/new/footer');

    }



    public function careers()
    {

        $data['title'] = 'Careers';
        $this->load->view('templates/new/header', $data);
        $this->load->view('pages/careers');
        $this->load->view('templates/new/footer');

    }


    public function home()
    {

        $data['title'] = 'Home';
        $this->load->view('templates/new/header', $data);
        $this->load->view('pages/home');
        $this->load->view('templates/new/footer');

    }


    public function betalist()
    {

        $data['title'] = 'Home';
        $this->load->view('templates/new/header', $data);
        $this->load->view('pages/betalist');
        $this->load->view('templates/new/footer');

    }


    public function producthunt()
    {

        $data['title'] = 'Home';
        $this->load->view('templates/new/header', $data);
        $this->load->view('pages/home');
        $this->load->view('templates/new/footer');

    }


    public function newbeta()
    {

        $data['title'] = 'Home';
        $this->load->view('templates/new/header', $data);
        $this->load->view('pages/beta-home');
        $this->load->view('templates/new/footer');

    }


    public function saas()
    {

        $data['title'] = 'Home';
        $this->load->view('templates/new/header', $data);
        $this->load->view('pages/business');
        $this->load->view('templates/new/footer');

    }

}