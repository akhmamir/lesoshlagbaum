<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function __construct()
        {
                parent::__construct();

                error_reporting(0);
                
                $username = $this->session->userdata('username');

                $this->load->helper('url');
                $bu = base_url();


                if(!$username){
                	redirect($bu.'auth/login');
                }



                $request_user = $this->db->get_where('users', array('email' => $username))->result();
				$start = $request_user[0]->start;
				if($start < 5) {
					redirect($bu.'/start');
					exit;
				}


        }

	public function index()
	{
		$username = $this->session->userdata('username');
   		$query = $this->db->get_where('users', array('email' => $username), $limit, $offset)->result();

		if($query[0]->photo) {
			$profile['photo'] = $query[0]->photo;
		}
		if($query[0]->name) {
			$profile['full_name'] = $query[0]->name;
		}

		$profile['username'] = $username;


		$data['title'] = 'Profile';
		$this->load->view('templates/header', $data);
		$this->load->view('user/profile', $profile);
		$this->load->view('templates/footer');
	}

	public function accounts()
	{
		$data['title'] = 'Manage accounts';
		$this->load->view('templates/header', $data);
		$this->load->view('user/accounts', $profile);
		$this->load->view('templates/footer');
	}

	public function alerts()
	{
		$data['title'] = 'Alerts';
		$this->load->view('templates/header', $data);
		$this->load->view('user/alerts', $profile);
		$this->load->view('templates/footer');
	}

	public function settings()
	{
		$data['title'] = 'Settings';
		$this->load->view('templates/header', $data);
		$this->load->view('user/settings', $profile);
		$this->load->view('templates/footer');
	}

	public function log()
	{
		$data['title'] = 'Login logs';
		$this->load->view('templates/header', $data);
		$this->load->view('user/log', $profile);
		$this->load->view('templates/footer');
	}

	public function protection()
	{
		$data['title'] = 'Protection';
		$this->load->view('templates/header', $data);
		$this->load->view('user/protection', $profile);
		$this->load->view('templates/footer');
	}

	public function subscription()
	{
		$data['title'] = 'Profile';
		$this->load->view('templates/header', $data);
		$this->load->view('user/subscription', $profile);
		$this->load->view('templates/footer');
	}









	public function get_accounts()
	{
		header('Content-Type: application/json');

		
		$username = $this->session->userdata('username');
		$query['module'] = 'accounts';

		$query['title'] = 'Your Accounts';
		$query['buttons'] = '<button class="btn btn-md mpg-button profile-service-status profile-action-button" style="float: right;"> <i class="icon ti-plus" aria-hidden="true"></i><span>Add new</span> </button>';
		$query['content'] = $this->db->get_where('accounts', array('user' => $username), $limit, $offset)->result();
		
		echo json_encode($query);
	}



	public function get_alerts()
	{
		header('Content-Type: application/json');
		
		$username = $this->session->userdata('username');
		$query['module'] = 'alerts';

		$query['title'] = 'Alerts';
		$query['buttons'] = '';
		$query['content'] = $this->db->get_where('alert', array('user' => $username), $limit, $offset)->result();
		echo json_encode($query);
	}



	public function get_protection()
	{
		header('Content-Type: application/json');
		
		$username = $this->session->userdata('username');
		$query['module'] = 'protection';

		$query['title'] = 'Protection';
		$query['buttons'] = '';
		$query['content'] = '<div class="protection-switch-block hidden"><div style="width:100%">This feature can be helpful if<br/>Kryptaxe is blocking you from logging in.</div><br/><br/><h3>Protection is </h3><div class="psb-switch"><button class="active-button">ON</button><button>OFF</button></div></div>';
		echo json_encode($query);
	}


	public function get_subscription()
	{
		header('Content-Type: application/json');
		
		$username = $this->session->userdata('username');
		$request_user = $this->db->get_where('users', array('email' => $username))->result();
		$get_limit = $request_user[0]->account_limit;

		$query['module'] = 'subscription';

		$query['title'] = 'Subscription';
		$query['buttons'] = '';

		if($get_limit == 1) {

			$query['content'] = '<div class="subscription-switch-block"><h3>You have a free account</h3><div class="ssb-switch"><button class="btn plan-upgrade-button">Upgrade</button></div>';

		} else {

			$query['content'] = '<div class="subscription-switch-block"><h3>Subscription is active</h3><div class="ssb-switch"><button class="active-button" disabled>Unsubscribe</button></div>';

		}

		echo json_encode($query);
	}



	public function get_settings()
	{
		header('Content-Type: application/json');
		
		$username = $this->session->userdata('username');
		$query = $this->db->get_where('users', array('email' => $username), $limit, $offset)->result();
 		
		$query['module'] = 'settings';

		$query['title'] = 'Settings';
		$query['buttons'] = '';
		$query['content'] = '<div class="profile-setting-block"><div class="psb-input-header">Your phone number</div><input type="text" value="'.$query[0]->secret_word.'" /><button class="number-save-button" style="width: 100%; height: 50px; background: #96d677; color: #fff; border: 0;">Save</button></div>';

		echo json_encode($query);
	}





	public function get_logs()
	{
		header('Content-Type: application/json');

		$username = $this->session->userdata('username');
		$query['name'] = 'Kryptaxe X Beta';
		$query['content'] = $this->db->get_where('accounts', array('user' => $username), $limit, $offset)->result();
		echo json_encode($query);
	}


	public function get_support()
	{
		header('Content-Type: application/json');
		
		$username = $this->session->userdata('username');
		$query['module'] = 'support';

		$query['title'] = 'Support';
		$query['buttons'] = '';
		$query['content'] = '<div style="font-size: 20px; font-weight: 600; line-height: 40px;">Support is available 9 to 18 EST time<br/>via email: support@kryptax.com<br/>via phone: +1 (929) 229-2549<br/> via live chat in a bottom right angle.</div>';
		echo json_encode($query);

	}




	public function beta()
	{
		header('Content-Type: application/json');

		//$username = $this->session->userdata('username');
		$query['module'] = 'beta';

		$query['title'] = 'Krypraxe X private beta';
		$query['buttons'] = '';
		$query['content'] = '<h2>Kryptaxe X is a next-level security for your accounts.</h2><br/><br/><br/><br/><h4>Facebook beta testing is currently in progress.</h4>';
		echo json_encode($query);
	}

	



	
}
