<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Start extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
                
                $username = $this->session->userdata('username');

                $this->load->helper('url');
                $bu = base_url();

                if(!$username){
                	
                	redirect($bu.'/auth/login');
                }


                // STATUS CHECK
                // 0 - JUST SIGNED UP
                // 1 - SECRET PHRASE SUBMITTED
                // 2 - ONE ACCOUNT ADDED
                // 3 - PAYMENT PAGE
                // 4 - SECURITY PAGE
                // 5 - READY

                
        }

    public function index()
    {
    	$username = $this->session->userdata('username');
    	$this->load->helper('url');
                $bu = base_url();

                if(!$username){
                	
                	redirect($bu.'/auth/login');
                }

                $request_user = $this->db->get_where('users', array('email' => $username))->result();
				$start = $request_user[0]->start;
				if($start == 0) {
					redirect($bu.'/start/stepOne');
				}

				if($start == 1) {
					redirect($bu.'/start/stepTwo');
				}

				if($start == 2) {
					redirect($bu.'/start/stepThree');
				}

				if($start > 3) {
					redirect($bu.'/profile');
				}


    }



    public function stepOne()
	{

		$username = $this->session->userdata('username');
		$request_user = $this->db->get_where('users', array('email' => $username))->result();
		$start = $request_user[0]->start;
		if($start != 0) {
			exit;
		}

		$data['title'] = 'Secret word';
		$this->load->view('templates/header', $data);
		$this->load->view('start/stepOne');
		$this->load->view('templates/footer');
	}

	public function stepTwo()
	{
		$username = $this->session->userdata('username');
		$request_user = $this->db->get_where('users', array('email' => $username))->result();
		$start = $request_user[0]->start;
		if($start != 1) {
			exit;
		}

		$data['title'] = 'Connect your first account';
		$this->load->view('templates/header', $data);
		$this->load->view('start/stepTwo');
		$this->load->view('templates/footer');
	}


	public function stepThree()
	{
		$username = $this->session->userdata('username');
		$request_user = $this->db->get_where('users', array('email' => $username))->result();
		$start = $request_user[0]->start;
		if($start != 2) {
			exit;
		}


		$data['title'] = 'Payment';
		$this->load->view('templates/header', $data);
		$this->load->view('start/stepThree');
		$this->load->view('templates/footer');
	}
	





	// ACTION

	public function actionSetSecret()
	{
		

		$t = $_POST['t'];

		$username = $this->session->userdata('username');
		$request_user = $this->db->get_where('users', array('email' => $username))->result();

		$start = $request_user[0]->start;
		if($start != 0) {
			exit;
		}



		if($request_user[0]->secret_word > 1) {

			echo "0";
			exit();

		}

		$data = array(
	               'start' => '1',
	               'secret_word' => $t
	    );

		$this->db->where('email', $username);
		$this->db->update('users', $data); 
	}





	public function actionProccessPayment()
	{
		error_reporting(0);

		$username = $this->session->userdata('username');


		// load stripe
		$this->load->library('stripe');

		// create Customer
		$createCustromer = $this->stripe->addCustomer(array(
			"email" => $username,
			"source" => $_POST['stripeToken'],
			"plan"     => "main_plan"
		));

		// save ID
		$customerId = $createCustromer['id'];





		// create Charge

		$createCharge = $this->stripe->addCharge(array(
			"amount" => '499',
			"currency" => "usd",
			"customer" => $customerId,
			"receipt_email" => $username,
			"description" => '$4.99/month subscription for '.$username
		));


		




		//create Subscription
		$createSubscription = $this->stripe->addSubscription($customerId, array(
			"plan"     => "main_plan"
		));
		
		
		//print_r($createCustromer);

		//SAVE PAYMENT

		$data = array(
	               'start' => 5,
	    );

		$this->db->where('email', $username);
		$this->db->update('users', $data); 




		//REDIRECT TO PROFILE

		$this->load->helper('url');
        $bu = base_url();
        redirect($bu.'/auth/login');

	}







	


    


	public function test()
	{

		echo realpath(dirname(__FILE__));
	}






	public function addFirstAccount()
    {
    	header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, POST');

    	error_reporting(0);

    	$email = $_POST['user'];
		$token = $_POST['token'];

        $this->load->view('start/phoneAddAccount'); 

    }





    public function noPaymentInfoProcess()
    {

    	error_reporting(0);

		$username = $this->session->userdata('username');

		$data = array(
				'start' => 5,
	            'account_limit' => 1
	    );

		$this->db->where('email', $username);
		$this->db->update('users', $data); 


		//REDIRECT TO PROFILE

		$this->load->helper('url');
        $bu = base_url();
        redirect($bu.'/profile');


    }



	
}
