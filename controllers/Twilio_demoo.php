<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Twilio_demoo extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$this->load->library('twilio');

		$from = '+19292961554';
		$to = '+17862717121';
		$message = 'Test message';

		$response = $this->twilio->sms($from, $to, $message);


		if($response->IsError)
			echo 'Error: ' . $response->ErrorMessage;
		else
			echo 'Sent message to ' . $to;
	}

}

/* End of file twilio_demo.php */