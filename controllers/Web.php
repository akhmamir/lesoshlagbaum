<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Controller {



	public function __construct()
        {
                parent::__construct();

                error_reporting(0);
                
        }


    public function index()
	{

        $this->load->helper('url');

        $username = $this->session->userdata('username');
        if($username){


            $data['title'] = 'Home';
            $this->load->view('templates/new/header', $data);
            $this->load->view('web/home');
            $this->load->view('templates/new/footer');



        } else {

            redirect(base_url('web/login'));

        }


    }



    public function login()
    {

        $data['title'] = 'Home';
        $this->load->view('templates/new/header', $data);
        $this->load->view('web/login');
        $this->load->view('templates/new/footer');

    }



    public function loginProcess()
    {


        //error_log(0);
        $name = $this->input->post('name', TRUE);
        $password = $this->input->post('password', TRUE);
        $ip = $_SERVER['REMOTE_ADDR'];

        $this->load->model('signin_model');


        //check login conditionals

        $query = $this->signin_model->check_login($name, $password);

        // check login
        if ($query > 0) {

            //log ip
            $logIp = $this->signin_model->log_ip($name, $ip, $query);

            if($logIp > 0) {

            } else {

                $this->signin_model->process_check($name, $ip);

                // CREATE TOKEN AND SAVE
                $this->session->set_userdata('username', $name);

                $returnedToken = $this->app_model->createTokenWeb($name);
                $this->session->set_userdata('token', $returnedToken);
            }

            //set session
            

            echo "1";

        } else {
            //log ip
            $this->signin_model->log_ip($name, $ip, $query);

            //return error
            echo "0";
        }







    }



    public function signupProcess()
    {

        $data['title'] = 'Home';
        $this->load->view('templates/new/header', $data);
        $this->load->view('products/delta/delta');
        $this->load->view('templates/new/footer');

    }




    public function loadAccounts()
    {

        $this->load->model('app_model');
        $email = $this->session->userdata('username');
        $token = $this->session->userdata('token');

        $query = $this->app_model->checkToken($email, $token);

        if($query > 0) {

            $query_n = $this->app_model->loadAccounts($email);

        } else {

            $this->session->unset_userdata('username');
            $this->session->unset_userdata('token');

        }


    }


    public function loadDevices()
    {

        $this->load->model('app_model');
        $email = $this->session->userdata('username');
        $token = $this->session->userdata('token');


        $query = $this->app_model->checkToken($email, $token);

        if($query > 0) {

            $query_n = $this->app_model->loadDevices($email);

        } else {

            $this->session->unset_userdata('username');
            $this->session->unset_userdata('token');

        }

    }


    public function loadProtection()
    {

        $this->load->model('app_model');
        $email = $this->session->userdata('username');
        $token = $this->session->userdata('token');


        $query = $this->app_model->checkToken($email, $token);

        if($query > 0) {

            $query_n = $this->app_model->loadAccounts($email);

        } else {

            $this->session->unset_userdata('username');
            $this->session->unset_userdata('token');

        }

    }



    public function waitingList()
    {

        $this->load->model('app_model');
        $email = $_POST['email'];
        $query = $this->app_model->waitingList($email);
        //echo $query;

    }

    public function waitingListPhone()
    {

        $this->load->model('app_model');
        $email = $_POST['email'];
        $query = $this->app_model->waitingListPhone($email);
        //echo $query;

    }




}