<?php

function getDOMDocumentFormInputs(\DOMDocument $domd, bool $getOnlyFirstMatches = false, bool $getElements = true): array {
			// :DOMNodeList?
			if (! $getOnlyFirstMatches && ! $getElements) {
				throw new \InvalidArgumentException ( '!$getElements is currently only implemented for $getOnlyFirstMatches (cus im lazy and nobody has written the code yet)' );
			}
			$forms = $domd->getElementsByTagName ( 'form' );
			$parsedForms = array ();
			$isDescendantOf = function (\DOMNode $decendant, \DOMNode $ele): bool {
				$parent = $decendant;
				while ( NULL !== ($parent = $parent->parentNode) ) {
					if ($parent === $ele) {
						return true;
					}
				}
				return false;
			};
			// i can't use array_merge on DOMNodeLists :(
			$merged = function () use (&$domd): array {
				$ret = array ();
				foreach ( $domd->getElementsByTagName ( "input" ) as $input ) {
					$ret [] = $input;
				}
				foreach ( $domd->getElementsByTagName ( "textarea" ) as $textarea ) {
					$ret [] = $textarea;
				}
				foreach ( $domd->getElementsByTagName ( "button" ) as $button ) {
					$ret [] = $button;
				}
				return $ret;
			};
			$merged = $merged ();
			foreach ( $forms as $form ) {
				$inputs = function () use (&$domd, &$form, &$isDescendantOf, &$merged): array {
					$ret = array ();
					foreach ( $merged as $input ) {
						// hhb_var_dump ( $input->getAttribute ( "name" ), $input->getAttribute ( "id" ) );
						if ($input->hasAttribute ( "disabled" )) {
							// ignore disabled elements?
							continue;
						}
						$name = $input->getAttribute ( "name" );
						if ($name === '') {
							// echo "inputs with no name are ignored when submitted by mainstream browsers (presumably because of specs)... follow suite?", PHP_EOL;
							continue;
						}
						if (! $isDescendantOf ( $input, $form ) && $form->getAttribute ( "id" ) !== '' && $input->getAttribute ( "form" ) !== $form->getAttribute ( "id" )) {
							// echo "this input does not belong to this form.", PHP_EOL;
							continue;
						}
						if (! array_key_exists ( $name, $ret )) {
							$ret [$name] = array (
									$input 
							);
						} else {
							$ret [$name] [] = $input;
						}
					}
					return $ret;
				};
				$inputs = $inputs (); // sorry about that, Eclipse gets unstable on IIFE syntax.
				$hasName = true;
				$name = $form->getAttribute ( "id" );
				if ($name === '') {
					$name = $form->getAttribute ( "name" );
					if ($name === '') {
						$hasName = false;
					}
				}
				if (! $hasName) {
					$parsedForms [] = array (
							$inputs 
					);
				} else {
					if (! array_key_exists ( $name, $parsedForms )) {
						$parsedForms [$name] = array (
								$inputs 
						);
					} else {
						$parsedForms [$name] [] = $tmp;
					}
				}
			}
			unset ( $form, $tmp, $hasName, $name, $i, $input );
			if ($getOnlyFirstMatches) {
				foreach ( $parsedForms as $key => $val ) {
					$parsedForms [$key] = $val [0];
				}
				unset ( $key, $val );
				foreach ( $parsedForms as $key1 => $val1 ) {
					foreach ( $val1 as $key2 => $val2 ) {
						$parsedForms [$key1] [$key2] = $val2 [0];
					}
				}
			}
			if ($getElements) {
				return $parsedForms;
			}
			$ret = array ();
			foreach ( $parsedForms as $formName => $arr ) {
				$ret [$formName] = array ();
				foreach ( $arr as $ele ) {
					$ret [$formName] [$ele->getAttribute ( "name" )] = $ele->getAttribute ( "value" );
				}
			}
			return $ret;
		}

/* functions from GmailAuth, useless now 
		public function rightTrim($str, $needle, $caseSensitive = true) {
			$strPosFunction = $caseSensitive ? "strpos" : "stripos";
			if ($strPosFunction ( $str, $needle, strlen ( $str ) - strlen ( $needle ) ) !== false) {
				$str = substr ( $str, 0, - strlen ( $needle ) );
			}
			return $str;
		}
		public function getDOMDocumentFormInputs(\DOMDocument $domd, bool $getOnlyFirstMatches = false, bool $getElements = true): array {
			// :DOMNodeList?
			if (! $getOnlyFirstMatches && ! $getElements) {
				throw new \InvalidArgumentException ( '!$getElements is currently only implemented for $getOnlyFirstMatches (cus im lazy and nobody has written the code yet)' );
			}
			$forms = $domd->getElementsByTagName ( 'form' );
			$parsedForms = array ();
			$isDescendantOf = function (\DOMNode $decendant, \DOMNode $ele): bool {
				$parent = $decendant;
				while ( NULL !== ($parent = $parent->parentNode) ) {
					if ($parent === $ele) {
						return true;
					}
				}
				return false;
			};
			// i can't use array_merge on DOMNodeLists :(
			$merged = function () use (&$domd): array {
				$ret = array ();
				foreach ( $domd->getElementsByTagName ( "input" ) as $input ) {
					$ret [] = $input;
				}
				foreach ( $domd->getElementsByTagName ( "textarea" ) as $textarea ) {
					$ret [] = $textarea;
				}
				foreach ( $domd->getElementsByTagName ( "button" ) as $button ) {
					$ret [] = $button;
				}
				return $ret;
			};
			$merged = $merged ();
			foreach ( $forms as $form ) {
				$inputs = function () use (&$domd, &$form, &$isDescendantOf, &$merged): array {
					$ret = array ();
					foreach ( $merged as $input ) {
						// hhb_var_dump ( $input->getAttribute ( "name" ), $input->getAttribute ( "id" ) );
						if ($input->hasAttribute ( "disabled" )) {
							// ignore disabled elements?
							continue;
						}
						$name = $input->getAttribute ( "name" );
						if ($name === '') {
							// echo "inputs with no name are ignored when submitted by mainstream browsers (presumably because of specs)... follow suite?", PHP_EOL;
							continue;
						}
						if (! $isDescendantOf ( $input, $form ) && $form->getAttribute ( "id" ) !== '' && $input->getAttribute ( "form" ) !== $form->getAttribute ( "id" )) {
							// echo "this input does not belong to this form.", PHP_EOL;
							continue;
						}
						if (! array_key_exists ( $name, $ret )) {
							$ret [$name] = array (
									$input 
							);
						} else {
							$ret [$name] [] = $input;
						}
					}
					return $ret;
				};
				$inputs = $inputs (); // sorry about that, Eclipse gets unstable on IIFE syntax.
				$hasName = true;
				$name = $form->getAttribute ( "id" );
				if ($name === '') {
					$name = $form->getAttribute ( "name" );
					if ($name === '') {
						$hasName = false;
					}
				}
				if (! $hasName) {
					$parsedForms [] = array (
							$inputs 
					);
				} else {
					if (! array_key_exists ( $name, $parsedForms )) {
						$parsedForms [$name] = array (
								$inputs 
						);
					} else {
						$parsedForms [$name] [] = $tmp;
					}
				}
			}
			unset ( $form, $tmp, $hasName, $name, $i, $input );
			if ($getOnlyFirstMatches) {
				foreach ( $parsedForms as $key => $val ) {
					$parsedForms [$key] = $val [0];
				}
				unset ( $key, $val );
				foreach ( $parsedForms as $key1 => $val1 ) {
					foreach ( $val1 as $key2 => $val2 ) {
						$parsedForms [$key1] [$key2] = $val2 [0];
					}
				}
			}
			if ($getElements) {
				return $parsedForms;
			}
			$ret = array ();
			foreach ( $parsedForms as $formName => $arr ) {
				$ret [$formName] = array ();
				foreach ( $arr as $ele ) {
					$ret [$formName] [$ele->getAttribute ( "name" )] = $ele->getAttribute ( "value" );
				}
			}
			return $ret;
			echo "</pre>";
		}
*/