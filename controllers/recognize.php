<?php

function recognize(
            $url,
            $apikey = '1c2e99cd93aec2b1dfdbbbc9a0ba10ff',
            $is_verbose = true,
            $domain="rucaptcha.com",
            $rtimeout = 5,
            $mtimeout = 120,
            $is_phrase = 0,
            $is_regsense = 0,
            $is_numeric = 0,
            $min_len = 0,
            $max_len = 0,
            $language = 0
            )
{
    $imagedata = file_get_contents("$url");
    $base64 = base64_encode($imagedata);
    $postdata = array(
        'method'    => 'base64', 
        'key'       => $apikey, 
        'body'      => $base64,
        'phrase'	=> $is_phrase,
        'regsense'	=> $is_regsense,
        'numeric'	=> $is_numeric,
        'min_len'	=> $min_len,
        'max_len'	=> $max_len,
		'language'	=> $language
        
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,             "http://$domain/in.php");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,     1);
    curl_setopt($ch, CURLOPT_TIMEOUT,             60);
    curl_setopt($ch, CURLOPT_POST,                 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,         $postdata);
    $result = curl_exec($ch);
    if (curl_errno($ch)) 
    {
    	if ($is_verbose) echo "CURL returned error: ".curl_error($ch)."\n";
        return false;
    }
    curl_close($ch);
    if (strpos($result, "ERROR")!==false)
    {
    	if ($is_verbose) echo "server returned error: $result\n";
        return false;
    }
    else
    {
        $ex = explode("|", $result);
        $captcha_id = $ex[1];
    	if ($is_verbose) /* echo "captcha sent, got captcha ID $captcha_id\n" */;
        $waittime = 0;
        if ($is_verbose) /*echo "waiting for $rtimeout seconds\n" */;
        sleep($rtimeout);
        while(true)
        {
            $result = file_get_contents("http://$domain/res.php?key=".$apikey.'&action=get&id='.$captcha_id);
            if (strpos($result, 'ERROR')!==false)
            {
            	if ($is_verbose) echo "server returned error: $result\n";
                return false;
            }
            if ($result=="CAPCHA_NOT_READY")
            {
            	if ($is_verbose) /*echo "captcha is not ready yet\n "*/;
            	$waittime += $rtimeout;
            	if ($waittime>$mtimeout) 
            	{
            		if ($is_verbose)/* echo "timelimit ($mtimeout) hit\n"*/;
            		break;
            	}
        		if ($is_verbose)/* echo "waiting for $rtimeout seconds\n"*/;
            	sleep($rtimeout);
            }
            else
            {
            	$ex = explode('|', $result);
            	if (trim($ex[0])=='OK') return trim($ex[1]);
            }
        }
        
        return false;
    }
}
?> 