<section class="page-title page-title-4 bg-secondary" style="background-image: linear-gradient(-225deg, #E3FDF5 0%, #FFE6FA 100%);">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="uppercase mb0">
                                Final step</h3>
                        </div>
                        <div class="col-md-6 text-right">
                            <div class="uppercase" style="font-size: 20px;">Step 3/3.</div>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
</section>
<section>
    <div class="container">
        <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 text-center">
            
            <h4 class="uppercase mb16">Everything is ready!</h4>
            <p class="lead mb80">
                Please make a payment to get Kryptaxe started.
            </p>
            

            <div  class="p_block" style="">
                <div class="payment-left">
                    <div style="text-align: center;">
                        <div class="d9dollars">$9<span style="font-size: 50px;">/mo</span></div>
                        <div style="color: #fff;margin-top: 40px;font-size: 19px;">Fixed price, unlimited accounts,<br />24/7 protection</div>
                        <div style="font-size: 45px; color: #fff;    margin-top: 40px;">
                            <i class="fa fa-cc-visa" aria-hidden="true"></i>
                            <i class="fa fa-cc-mastercard" aria-hidden="true"></i>
                            <i class="fa fa-cc-amex" aria-hidden="true"></i>
                            <i class="fa fa-cc-discover" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
                <div class="payment" style="float: right;background: #fbfbfb;">

                      <form action="process.php" method="POST" novalidate="" autocomplete="on" id="payment-form">

                        <label>Card Number:</label>
                        <input type="text" id="amount_sum" name="amount" style="display:none">
                        <input type="text" id="credit-input" class="cc-number" pattern="\d*" x-autocompletetype="cc-number" placeholder="•••• •••• •••• ••••" required="">

                        <label>Expiration date:</label>
                        <div style="overflow:overlay">
                            <select class="card-expiry-month" pattern="\d*" x-autocompletetype="cc-exp" placeholder="MM" required="" style="width:50%">
                            <option value="0">MM</option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>

                            <select class="card-expiry-year" pattern="\d*" x-autocompletetype="cc-exp" placeholder="YYYY" required="" title="YY" style="width:50%">
                             <option value="0">YYYY</option>
                                <option value="2016">2016</option>
                                <option value="2017">2017</option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                                <option value="2026">2026</option>
                                <option value="2027">2027</option>
                                <option value="2028">2028</option>
                                <option value="2029">2029</option>
                                <option value="2030">2030</option>
                                <option value="2031">2031</option>
                            </select>
                        </div>

                        <label style="margin-top: 12px;">CVC / CVV:</label>
                        <input type="text" class="cc-cvc" pattern="\d*" x-autocompletetype="cc-csc" placeholder="123" required="" maxlength="4" autocomplete="off">

                        <label class="validation"></label>

                        <button class="payment_button" type="submit" style="box-shadow: 0px 2px 20px #9797ff;margin-bottom: 0; margin-top: 20px;">Subscribe <i class="fa fa-credit-card" aria-hidden="true"></i></button>
                      </form>

                    </div>
            </div>

            <div class="coupon">
                Want to get 1 month free? Share this on Twitter or Facebook, and we'll refund your first month payment
            </div>

        </div>
    </div>
</section>
<style type="text/css">
    
/*payment*/
.payment-left {float: left;width: calc(100% - 360px);height: 392px;background: #000000;background-image: linear-gradient(to top, #fbc2eb 0%, #a6c1ee 100%);}

::-webkit-input-placeholder { /* Chrome/Opera/Safari */
  font-size: 20px
}


.p_block {text-align:left;width: 800px;overflow: overlay;box-shadow: 0px 0px 50px #e3e3e3;margin: 0 auto;}
.d9dollars {color: #fff;font-size: 80px;padding-top: 118px;    text-shadow: 0px 2px 5px #a5a5a5;}
.e_HH{font-size: 20px}
.mng_contactu{font-size: 13px;    margin-top: 20px;}

.payment {width: 360px; overflow: overlay;margin: 0 auto; text-align: left; background: #d6ffd9; padding: 20px;}
.payment button {width: 100%;}
.payment input{width: calc(100% - 25px);}

[placeholder]::-webkit-input-placeholder { color: rgba(0,0,0,.3); }
[placeholder]:hover::-webkit-input-placeholder { color: rgba(0,0,0,.15); }
[placeholder]:focus::-webkit-input-placeholder { color: transparent; }

[placeholder]::-moz-placeholder { color: rgba(0,0,0,.3); }
[placeholder]:hover::-moz-placeholder { color: rgba(0,0,0,.15); }
[placeholder]:focus::-moz-placeholder { color: transparent; }

[placeholder]:-ms-input-placeholder { color: rgba(0,0,0,.3); }
[placeholder]:hover:-ms-input-placeholder { color: rgba(0,0,0,.15); }
[placeholder]:focus:-ms-input-placeholder { color: transparent; }

button::-moz-focus-inner,
input::-moz-focus-inner{
  border: 0;
  padding: 0;
}

.payment label, .payment input, .payment button {
  display: block;
  margin: 0 auto 10px;
}

label {
  font-size: 13px;
  font-weight: 600;
  color: #777;
}



input.invalid {
  border: 1px solid red;
}

.validation.failed:after {
  color: red;
  content: 'Validation failed';
}

.validation.passed:after {
  color: green;
  content: 'Validation passed';
}

.features{display:none;position: fixed;
    top: 17vh;
    left: 0;
    color: #fff;
    right: 0;
    bottom: 0;
    margin: auto;
    overflow-y: overlay;width: 600px}
.f_header{font-size: 34px;font-weight: 100;text-align: center;}
.f_f_h{color: #c7ac75;}
.f_f_t{color: #797979;}
.f_f_item{margin-top: 20px;
    margin-bottom: 20px;}
.back{display:none;background: rgba(0, 0, 0, 0.95); top: 0; left: 0; width: 100%; height: 100%; position: fixed;}

.b-close{text-align: center}

#credit-input {background: #fff; width: 100%; font-size: 20px; letter-spacing: 3px;    font-family: helvetica;}
.card-expiry-month, .card-expiry-year {width: 50%;float: left;background: #fff;margin-bottom: 8px;    font-family: helvetica;}
.cc-cvc {width: 100%  !important; background: #fff !important;    font-family: helvetica;}
.payment_button {box-shadow: 0px 2px 20px #9797ff !important; background: blue !important; border: blue !important; font-size: 18px  !important;}



@media (max-width: 740px) {
    #chat-application{display: none !important;}
    .main_info {font-size: 30px !important;}
    .main_info button{margin-top: 40px;}
    .work{width: 100% !important;    margin-bottom: 5px;}
    .work img{opacity: 1}
    .payment{width: calc(100% - 40px);}
    .email_form{    width: 100%;}
    .email_form input {    width: calc(100% - 25px);}
    .email_form button {width: 100%;}
    .hp_c{    width: 100%;margin-bottom: 20px;border-bottom: 1px solid #e3e3e3; padding-bottom: 20px;}
    .w_d_button {width: calc(106% - 90px);margin-bottom: 20px;}
    .ww_b{width: calc(100% - 40px);,margin-bottom: 20px}
    .logo{font-size: 24px;}
    .features{width: 86%; top: 0; padding: 10px; word-wrap: break-word; margin: 0;}
}
</style>

<script type="text/javascript" src="https://js.stripe.com/v1/"></script>


<script type="text/javascript">
        function cc_format(value) {
          var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '')
          var matches = v.match(/\d{4,16}/g);
          var match = matches && matches[0] || ''
          var parts = []
          for (i=0, len=match.length; i<len; i+=4) {
            parts.push(match.substring(i, i+4))
          }
          if (parts.length) {
            return parts.join(' ')
          } else {
            return value
          }
        }

        onload = function() {
          document.getElementById('credit-input').oninput = function() {
            this.value = cc_format(this.value)
          }
        }
    </script>