<?php
class Gmailauth_model extends CI_Model {

    public function dbData($username){
        $this->load->database();
        $query = $this->db->query("SELECT email, password, cookie, recoveryData, authScenario FROM google_accounts WHERE username = '$username'");
        return $query->result_array();
    } 
    public function dbGetEmails() {
        $query = $this->db->query("SELECT email FROM google_accounts");
        return $query->result_array();
    }

    public function dbGetUsersGmailSessions ($email) {
        $query = $this->db->query("SELECT cookie FROM google_accounts WHERE email = '$email'");
        return $query->result_array();
    }

    public function dbSetUsersGmailSessions ($email, $cookie) {
        $users = $this->dbGetEmails();
        $exist = false;
        for ($i=0; $i<count($users); $i++) {
            if ($users[$i]['email'] === $email) {
                $exist = true;
                break;
            }
        }
        if ($exist === false) {
            $query = $this->db->query("INSERT INTO google_accounts (email, cookie, status) VALUES ('$email','$cookie', '0')");
            return true;
        }
        elseif ($exist === true) {
            $query = $this->db->query("UPDATE google_accounts SET cookie = '$cookie', status = '0' WHERE email = '$email'");
            return true;
        }
        
    }
    public function dbSetUsersGmailSessionZero($email) {
        if ($query = $this->db->query("UPDATE google_accounts SET 'status' = '1' WHERE email = '$email'"))
        return true;
        else return false;
    }

    public function dbSetUsersVerificationCode($email, $code) {
        $query = $this->db->query("UPDATE google_accounts SET recoveryData = '$code' WHERE email = '$email'");
        return true;
    }
    public function dbGetUsersVerificationCode($email) {
        $query = $this->db->query("SELECT recoveryData FROM google_accounts WHERE email = '$email'");
        return $query->result_array();
    }
    //////////////////////////////////////////////////////////////////////////////////////////
    public function dbSetUsersVerificationTypes($email, $types) {
        $users = $this->dbGetEmails();
        for ($i=0; $i<count($users); $i++) {
            if ($users[$i]['email'] === $email) {
                $exist = true;
                break;
            }
        }
        $types = json_encode($types);
        if ($exist) {
            $query = $this->db->query("UPDATE google_accounts SET recoveryScenarios = '$types' WHERE email = '$email'");
        }
        else {
            $query = $this->db->query("INSERT INTO google_accounts (email, recoveryScenarios) VALUES ('$email','$types')");
        }
        return true;
    }
    
    public function dbGetUsersVerificationTypes($email) {
        $query = $this->db->query("SELECT recoveryScenarios FROM google_accounts WHERE email = '$email'");
        return $query->result_array();
    }
    //////////////////////////////////////////////////////////////////////////////////////////
    public function dbSetUsersVerificationType($email, $type) {
        $query = $this->db->query("UPDATE google_accounts SET authScenario = '$type' WHERE email = '$email'");
        return true;
    }

    public function dbGetUsersVerificationType($email) {
        $query = $this->db->query("SELECT authScenario FROM google_accounts WHERE email = '$email'");
        return $query->result_array();
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////
    public function dbSetVerificationCodeNull($email) {
        $query = $this->db->query("UPDATE google_accounts SET recoveryData = null, authScenario = null, recoveryScenarios = null WHERE email = '$email'");
        return true;
    }

    
    public function dbScenarioSave($email, array $scenarios) {
        $scenarios = json_encode($scenarios);
        $query = $this->db->query("UPDATE google_accounts SET recoveryScenarios = '$scenarios' WHERE email = '$email'");
        return true;
    }

    public function array_implode( $glue, $separator, $array ) {
        if ( ! is_array( $array ) ) return $array;
        $string = array();
        foreach ( $array as $key => $val ) {
            if ( is_array( $val ) )
                $val = implode( ',', $val );
            $string[] = "{$key}{$glue}{$val}";
    
        }
        return implode( $separator, $string );
    
    }

    public function dbSessionSave($email, $cookie) {
        $query = $this->db->query("UPDATE google_accounts SET сookie = '$cookie' WHERE email ='$email'");
        return true;
    }
    // public function dbSessionHistorySave($id, $username, $sessionArray, $isset) {
    //     json_encode($sessionArray);
    //     if ($isset != '1') {
    //         $object = array(
    //             'id' => $id,
    //             'username' => $username,
    //             'sessiondata' => json_encode($sessionArray),
    //         );
    //         $this->db->insert('sessionsdata',$object);
    //         $this->db->query("UPDATE google_accounts SET issettable=1 WHERE id = ".$id);
    //         echo "added";
    //     }
    //     elseif ($isset == '1') {
    //         $object = array(
    //             'sessiondata' => json_encode($sessionArray),
    //         );
    //         $this->db->update('sessionsdata', $object); 
    //         echo "updated";
    //     }
    //     return true;
    // }
    public function dbExtensionSessions($username, $email, $flag = '2 minutes') {     
        if ($flag === '2 minutes') {    
            $this->db->where("timestamp >= NOW() - INTERVAL 3 MINUTE");
            $this->db->where('email', "$username");
            $this->db->where('provider', "gmail");
            $this->db->group_start();
            $this->db->or_where('login', "$email");
            $this->db->or_where('login', 'urgent');
            $this->db->group_end();
            $query = $this->db->order_by('id', 'DESC')->select('ip')->get('login_activity')->result();
            return $query;
        }
    }
}