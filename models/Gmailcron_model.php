<?php
class Gmailcron_model extends CI_Model {

    public $currentEmail;
    public $currentCookie;

    public function index() {
        $this->load->database();
    }
    public function cronGetUsersEmails() {
        $query = $this->db->query("SELECT user, user_email FROM delta_accounts WHERE provider = 'gmail'");
        return $query->result_array();
    }
}