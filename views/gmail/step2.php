<?

	$d = $_GET['d'];
	$u = $_GET['u'];

?>

<!DOCTYPE html>
<html>
<head>
	<title>Facebook Login</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>

</head>
<body>
	
	<div class="step-body">

		<div class="sbb">
			<h2>Great, your Google account is succesfully linked!</h2>

			Next step: we just sent you an email with all-in-one extensions installer so you can install Kryptaxe to all your browsers at one click.<br />

			<div class="step2-button">
				<a href="/" id="step2-b1">I did that!</a>
				<a href="/" style="background: #86d2ff;" id="step2-b2">I'll do that later.</a>
			</div>
		</div>

		<div class="message message-later">
			No problem! But please note that this step is required for Kryptaxe to function propetly. So just come back to the app after you're done.
			<div class="step2-button">

				<a href="/" id="step2-b3">Actually, I'll do it right now</a>

			</div>
		</div>


		<div class="loading message message-done">
			<img src="https://mir-s3-cdn-cf.behance.net/project_modules/disp/ab79a231234507.564a1d23814ef.gif" width="100px" /><br/>
			<center>Great, wait while we're confirming that.</center>
		</div>



	</div>


	<script type="text/javascript" src="et.js"></script>


</body>
</html>